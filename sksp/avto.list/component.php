<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$requiredModules = array('highloadblock');
foreach ($requiredModules as $requiredModule)
{
	if (!CModule::IncludeModule($requiredModule))
	{
		ShowError(GetMessage("F_NO_MODULE"));
		return 0;
	}
}
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

// hlblock info
$hlblock_id = $arParams['BLOCK_ID'];

// Функция для проверки доступности автомобиля
function isCarAvailable($carStartTime, $carEndTime, $desiredStart, $desiredEnd) {
    $start = strtotime($carStartTime);
    $end = strtotime($carEndTime);
    $desiredStart = strtotime($desiredStart);
    $desiredEnd = strtotime($desiredEnd);

    // Если автомобиль не забронирован вообще
    if ($carStartTime === null && $carEndTime === null) {
        return true;
    }

    // Если запрашиваемое время начала и окончания не пересекается с временем бронирования
    if ($desiredEnd <= $start || $desiredStart >= $end) {
        return true;
    }

    return false;
}

//запрашиваемое время начала бронирования
$bookingStart = filter_input(INPUT_GET, $arParams["BOOKING_START"], FILTER_SANITIZE_STRING);
$bookingStart = htmlspecialchars($bookingStart, ENT_QUOTES, 'UTF-8');

//запрашиваемое время окочнания бронирования
$bookingEnd = filter_input(INPUT_GET, $arParams["BOOKING_END"], FILTER_SANITIZE_STRING);
$bookingEnd = htmlspecialchars($bookingEnd, ENT_QUOTES, 'UTF-8');

$arCategoryComfort = array();

//получаем доступные для нашей группы пользователя категории комфорта
global $USER;
$userId = $USER->GetID();

// Определяем права доступа пользователя
$arGroups = CUser::GetUserGroup($userId);
$arFilter = [
	"IBLOCK_ID" => $arParams['IBLOCK_ID'],
    "ACTIVE" => "Y",
    "CHECK_PERMISSIONS" => "Y", // Проверяем права доступа
    "GROUPS" => $arGroups // Группы текущего пользователя
];

// Получаем элементы инфоблока
$res = CIBlockElement::GetList([], $arFilter);
while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    // Собираем массив доступных категорий комфорта
    $arCategoryComfort[] = $arFields['ID'];
}
if (empty($hlblock_id))
{
	ShowError(GetMessage('AVTO_LIST_NO_ID'));
	return 0;
}
$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
if (empty($hlblock))
{
	ShowError(GetMessage('AVTO_LIST_404'));
	return 0;
}

$entity = HL\HighloadBlockTable::compileEntity($hlblock);

// uf info
$fields = $GLOBALS['USER_FIELD_MANAGER']->GetUserFields('HLBLOCK_'.$hlblock['ID'], 0, LANGUAGE_ID);

// sort
$sortId = 'ID';
$sortType = 'DESC';
if (isset($arParams['SORT_FIELD']) && isset($fields[$arParams['SORT_FIELD']]))
{
	$sortId = $arParams['SORT_FIELD'];
}
// for compatibility
elseif (isset($_GET['sort_id']) && isset($fields[$_GET['sort_id']]))
{
	$sortId = $_GET['sort_id'];
}
if (isset($arParams['SORT_ORDER']) && in_array($arParams['SORT_ORDER'], array('ASC', 'DESC'), true))
{
	$sortType = $arParams['SORT_ORDER'];
}
// for compatibility
if (isset($_GET['sort_type']) && in_array($_GET['sort_type'], array('ASC', 'DESC'), true))
{
	$sortType = $_GET['sort_type'];
}

// start query
$mainQuery = new Entity\Query($entity);
$mainQuery->setSelect(array('*'));
$mainQuery->setOrder(array($sortId => $sortType));

//отфильтровываем элементы с разрешенной категорией комфорта
$GLOBALS['filterMain'] = array($arParams['PROP_COMFORT']=>$arCategoryComfort);
$filerBlock = 'filterMain';
// filter
if (
	isset($filerBlock) &&
	!empty($filerBlock) &&
	preg_match('/^[A-Za-z_][A-Za-z01-9_]*$/', $filerBlock))
{
	global ${$filerBlock};
	$filter = ${$filerBlock};
	if (is_array($filter))
	{
		$mainQuery->setFilter($filter);
	}
}


// execute query
//	->setGroup($group)
//	->setOptions($options);
$result = $mainQuery->exec();
$result = new CDBResult($result);

// build results
$rows = array();
$tableColumns = array();
while ($row = $result->fetch())
{
	$booking = false;
	$arFilter = [
	"IBLOCK_ID" => $arParams['BOOKING_IBLOCK_ID'],
    "ACTIVE" => "Y",
    "PROPERTY_".$arParams["PROP_BOOKING_AVTO"] => $row["ID"],
	];

	// Получаем элементы инфоблока
	$res = CIBlockElement::GetList([], $arFilter, array("PROPERTY_TIME_START", "PROPERTY_TIME_END", "ID"));
	while($ob = $res->GetNextElement()) {
	    $arFields = $ob->GetFields();
	    // Выводим информацию об элементе
	    
	    $arFields['PROPERTY_TIME_START_VALUE'] = str_replace(" ", "T", $arFields['PROPERTY_TIME_START_VALUE']);
	    $arFields['PROPERTY_TIME_START_VALUE'] = str_replace(".", "-", $arFields['PROPERTY_TIME_START_VALUE']);
	    $arFields['PROPERTY_TIME_END_VALUE'] = str_replace(" ", "T", $arFields['PROPERTY_TIME_END_VALUE']);
	    $arFields['PROPERTY_TIME_END_VALUE'] = str_replace(".", "-", $arFields['PROPERTY_TIME_END_VALUE']);

	    if(isCarAvailable($arFields['PROPERTY_TIME_START_VALUE'], $arFields['PROPERTY_TIME_END_VALUE'], $bookingStart, $bookingEnd)){
	    	$booking = false;
	    }else{
	    	$booking = true;
	    }
	}
	if($booking){
		continue;
	}

	foreach ($row as $k => $v)
	{
		$arUserField = $fields[$k];

		if ($k == 'ID')
		{
			$tableColumns['ID'] = true;
			continue;
		}
		if ($arUserField['SHOW_IN_LIST'] != 'Y')
		{
			continue;
		}

		$html = call_user_func_array(
			array($arUserField['USER_TYPE']['CLASS_NAME'], 'getadminlistviewhtml'),
			array(
				$arUserField,
				array(
					'NAME' => 'FIELDS['.$row['ID'].']['.$arUserField['FIELD_NAME'].']',
					'VALUE' => htmlspecialcharsbx(is_array($v) ? implode(', ', $v) : $v)
				)
			)
		);

		$tableColumns[$k] = true;
		$row[$k] = $html;
	}

	$rows[] = $row;
}

$arResult['rows'] = $rows;
$arResult['fields'] = $fields;
$arResult['tableColumns'] = $tableColumns;
$arResult['sort_id'] = $sortId;
$arResult['sort_type'] = $sortType;

// for compatibility
$arResult['NAV_STRING'] = '';
$arResult['NAV_PARAMS'] = '';
$arResult['NAV_NUM'] = 0;

$this->IncludeComponentTemplate();