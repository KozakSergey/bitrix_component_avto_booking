<?
$MESS["AVTO_COMPONENT_BLOCK_ID_PARAM"] = "Highload block ID";
$MESS['AVTO_COMPONENT_SORT_FIELD_PARAM'] = 'Sorting field';
$MESS['AVTO_COMPONENT_SORT_ORDER_PARAM'] = 'Sorting direction';
$MESS['AVTO_COMPONENT_SORT_ORDER_PARAM_DESC'] = 'Descending order';
$MESS['AVTO_COMPONENT_SORT_ORDER_PARAM_ASC'] = 'Ascending order';
$MESS['BOOKING_START'] = 'The variable in which the booking start time is passed';
$MESS['BOOKING_END'] = 'The variable in which the end time of the reservation is transmitted';
$MESS['AVTO_COMPONENT_LIST_TYPE'] = 'Type of information block for car comfort categories';
$MESS['AVTO_COMPONENT_LIST_ID'] = 'Information block of car comfort categories';
$MESS['AVTO_COMPONENT_PROP_COMFORT'] = 'Specify the property where the comfort category "UF" is stored_*';
$MESS['AVTO_COMPONENT_PROP_BOOKING_AVTO'] = 'Specify the property where the car id is stored in the booking information block';
$MESS['AVTO_COMPONENT_BOOKING_ID'] = 'Booking information block';
?>