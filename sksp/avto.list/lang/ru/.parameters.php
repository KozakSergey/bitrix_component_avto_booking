<?php
$MESS['AVTO_COMPONENT_BLOCK_ID_PARAM'] = 'ID highload блока';
$MESS['AVTO_COMPONENT_SORT_FIELD_PARAM'] = 'Поле сортировки';
$MESS['AVTO_COMPONENT_SORT_ORDER_PARAM'] = 'Направление сортировки';
$MESS['AVTO_COMPONENT_SORT_ORDER_PARAM_DESC'] = 'По убыванию';
$MESS['AVTO_COMPONENT_SORT_ORDER_PARAM_ASC'] = 'По возрастанию';
$MESS['BOOKING_START'] = 'Переменная в которой передается время начала бронирования';
$MESS['BOOKING_END'] = 'Переменная в которой передается время окончания бронирования';
$MESS['AVTO_COMPONENT_LIST_TYPE'] = 'Тип инфоблока категорий комфорта авто';
$MESS['AVTO_COMPONENT_LIST_ID'] = 'Инфоблок категорий комфорта авто';
$MESS['AVTO_COMPONENT_PROP_COMFORT'] = 'Укажите свойство в котором хранится категория комфорта "UF_*';
$MESS['AVTO_COMPONENT_PROP_BOOKING_AVTO'] = 'Укажите свойство в котором хранится id авто в инфоблоке бронирований';
$MESS['AVTO_COMPONENT_BOOKING_ID'] = 'Инфоблок бронирований';