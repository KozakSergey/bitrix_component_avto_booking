<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));
$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = "[".$arRes["ID"]."] ".$arRes["NAME"];
$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(
		'BLOCK_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('AVTO_COMPONENT_BLOCK_ID_PARAM'),
			'TYPE' => 'TEXT'
		),
		'SORT_FIELD' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('AVTO_COMPONENT_SORT_FIELD_PARAM'),
			'TYPE' => 'TEXT',
			'DEFAULT' => 'ID'
		),
		'SORT_ORDER' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('AVTO_COMPONENT_SORT_ORDER_PARAM'),
			'TYPE' => 'LIST',
			'DEFAULT' => 'DESC',
			'VALUES' => array(
				'DESC' => GetMessage('AVTO_COMPONENT_SORT_ORDER_PARAM_DESC'),
				'ASC' => GetMessage('AVTO_COMPONENT_SORT_ORDER_PARAM_ASC')
			)
		),
		"BOOKING_START" => array(
			"PARENT" => "ACTION_SETTINGS",
			"NAME"		=> GetMessage("BOOKING_START"),
			"TYPE"		=> "STRING",
			"DEFAULT"	=> "booking-start"
		),
		"BOOKING_END" => array(
			"PARENT" => "ACTION_SETTINGS",
			"NAME"		=> GetMessage("BOOKING_END"),
			"TYPE"		=> "STRING",
			"DEFAULT"	=> "booking-end"
		),
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("AVTO_COMPONENT_LIST_TYPE"),
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "news",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("AVTO_COMPONENT_LIST_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"BOOKING_IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("AVTO_COMPONENT_BOOKING_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"PROP_COMFORT" => array(
			"PARENT" => "ACTION_SETTINGS",
			"NAME"		=> GetMessage("AVTO_COMPONENT_PROP_COMFORT"),
			"TYPE"		=> "STRING",
			"DEFAULT"	=> ""
		),
		"PROP_BOOKING_AVTO" => array(
			"PARENT" => "ACTION_SETTINGS",
			"NAME"		=> GetMessage("AVTO_COMPONENT_PROP_BOOKING_AVTO"),
			"TYPE"		=> "STRING",
			"DEFAULT"	=> ""
		),
	),
);