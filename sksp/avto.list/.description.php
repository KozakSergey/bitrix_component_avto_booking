<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage('AVTO_COMPONENT_NAME'),
	"DESCRIPTION" => GetMessage('AVTO_COMPONENT_DESCRIPTION'),
	"ICON" => "",
	"CACHE_PATH" => "Y",
	"SORT" => 10,
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "hlblock",
			"NAME" => GetMessage('AVTO_COMPONENT_CATEGORY_TITLE'),
			"CHILD" => array(
				"ID" => "hlblock_list",
			),
		),
	),
);

?>